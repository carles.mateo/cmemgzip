from cmemgzip import Cmemgzip, StringUtils, ArgsUtils, FileUtils, DateTimeUtils, ScreenUtils, PythonUtils
import pytest
import gzip

class TestCmemgzip:

    def test_get_percent_compressed(self):

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils()
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        a_to_assert = [ [50, 100, 2, "50.00%"],
                        [11, 100, 2, "11.00%"],
                        [50, 111, 2, "45.05%"],
                        [50, 111, 4, "45.0450%"],
                        [450, 1024*1024, 2, "0.04%"],
                        [450, 1024 * 1024, 4, "0.0429%"],
                        [1024*1024*1024*1024, 7*1024*1024*1024*1024, 2, "14.29%"],
                        [1024*1024*1024*1024, 7*1024*1024*1024*1024, 3, "14.286%"]]

        for a_values in a_to_assert:
            i_bytes_compressed = a_values[0]
            i_bytes_read = a_values[1]
            i_decimal_positions = a_values[2]
            s_percent = a_values[3]

            s_compression_percent = o_cmemgzip.get_percent_compressed(i_bytes_compressed=i_bytes_compressed,
                                                                      i_bytes_read=i_bytes_read,
                                                                      i_decimal_positions=i_decimal_positions)
            assert s_compression_percent == s_percent

    def test_get_how_many_blocks(self):

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils()
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        assert o_cmemgzip.get_how_many_blocks(i_size_bytes=10*1024*1024*1024, i_block_size_mb=1) == 10240
        assert o_cmemgzip.get_how_many_blocks(i_size_bytes=((10*1024*1024*1024) + 1), i_block_size_mb=1) == 10241
        assert o_cmemgzip.get_how_many_blocks(i_size_bytes=(2.7 * 1024 * 1024 * 1024), i_block_size_mb=250) == 12
        assert o_cmemgzip.get_how_many_blocks(i_size_bytes=(8 * 1024 * 1024), i_block_size_mb=1) == 8
        assert o_cmemgzip.get_how_many_blocks(i_size_bytes=((8 * 1024 * 1024) + 256), i_block_size_mb=1) == 9

    def test_verify_file_size_min_value_or_exit_valid_values(self):

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils()
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        assert o_cmemgzip.verify_file_size_min_value_or_exit(100) is True
        assert o_cmemgzip.verify_file_size_min_value_or_exit(110) is True
        assert o_cmemgzip.verify_file_size_min_value_or_exit(1000000000000000000000) is True

    def test_verify_file_size_min_value_or_exit_invalid_values(self):
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils()
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            o_cmemgzip.verify_file_size_min_value_or_exit(90)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            o_cmemgzip.verify_file_size_min_value_or_exit(0)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

    def test_verify_access_and_get_filesize_or_exit_filesize_ok(self, monkeypatch):
        s_filename = "somefile.txt"

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils()
        o_screenutils = ScreenUtils(b_support_colors=True)

        def mock_fileutils_get_file_size_in_bytes_return(o_fileutils, s_file):
            # We return the bytes of the imaginary file
            b_result = True
            i_size_in_bytes = 1024

            return b_result, i_size_in_bytes

        monkeypatch.setattr(FileUtils, 'get_file_size_in_bytes', mock_fileutils_get_file_size_in_bytes_return)


        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)
        i_filesize = o_cmemgzip.verify_access_and_get_filesize_or_exit(s_file=s_filename)

        assert i_filesize == 1024

    def test_verify_access_and_get_filesize_or_exit_filesize_ko(self, monkeypatch):
        s_filename = "somefile.txt"

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils()
        o_screenutils = ScreenUtils(b_support_colors=True)

        def mock_fileutils_get_file_size_in_bytes_return(o_fileutils, s_file):
            # We return the bytes of the imaginary file
            b_result = False
            i_size_in_bytes = 0

            return b_result, i_size_in_bytes

        monkeypatch.setattr(FileUtils, 'get_file_size_in_bytes', mock_fileutils_get_file_size_in_bytes_return)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            i_filesize = o_cmemgzip.verify_access_and_get_filesize_or_exit(s_file=s_filename)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

    def test_validate_compressed_size_or_exit_ok(self):

        a_test_success = [ [2096, 1024],
                           [2837878, 1],
                           [1024, 1023],
                           [20969348294, 10249239],
                           [2096928390429832908320, 1024],
                           [3498549532096357897575878578, 102],
                           [150, 10]
                           ]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils()
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        for a_sizes in a_test_success:
            i_bytes_read = a_sizes[0]
            i_bytes_compressed = a_sizes[1]

            b_success = o_cmemgzip.verify_compressed_size_or_exit(i_bytes_read=i_bytes_read, i_bytes_compressed=i_bytes_compressed)

            assert b_success is True

    def test_validate_compressed_size_or_exit_not_ok(self):

        a_test_success = [ [2096, 3000],
                           [1, 2837878],
                           [1023, 1024],
                           [1024, 1024],  # @TODO: WiP. keep this
                           [10249239, 20969348294],
                           [1024, 2096928390429832908320],
                           [102, 3498549532096357897575878578],
                           [10, 150],
                           [1000000, 0]
                           ]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils()
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        for a_sizes in a_test_success:
            i_bytes_read = a_sizes[0]
            i_bytes_compressed = a_sizes[1]

            with pytest.raises(SystemExit) as pytest_wrapped_e:
                b_success = o_cmemgzip.verify_compressed_size_or_exit(i_bytes_read=i_bytes_read, i_bytes_compressed=i_bytes_compressed)

            assert pytest_wrapped_e.type == SystemExit
            assert pytest_wrapped_e.value.code == 1

    def test_validate_parameters_or_exit_parameters_ok(self):

        a_s_parameters = ["cmemgzip", "myfile.txt"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        b_simulate, i_mem_mb, s_filename, a_files = o_cmemgzip.validate_parameters_or_exit()

        assert b_simulate is False
        assert i_mem_mb == 0
        assert s_filename == "myfile.txt"
        assert len(a_files) == 1

    def test_validate_parameters_or_exit_parameters_3_files_ok(self):

        a_s_parameters = ["cmemgzip", "myfile.txt", "myfile2.txt", "myfile3.txt"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        b_simulate, i_mem_mb, s_filename, a_files = o_cmemgzip.validate_parameters_or_exit()

        assert b_simulate is False
        assert i_mem_mb == 0
        assert s_filename == "myfile.txt"
        assert len(a_files) == 3

    def test_validate_parameters_or_exit_parameters_asterisk_ok(self):

        a_s_parameters = ["cmemgzip", "*"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        b_simulate, i_mem_mb, s_filename, a_files = o_cmemgzip.validate_parameters_or_exit()

        assert b_simulate is False
        assert i_mem_mb == 0
        assert s_filename == "*"
        assert len(a_files) == 1

    def test_validate_parameters_or_exit_no_parameters(self):
    
        a_s_parameters = []

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            b_simulate, i_mem_mb, s_filename = o_cmemgzip.validate_parameters_or_exit()

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

        # It will not get to here, an exit will be triggered first
        # assert b_simulate is False
        # assert i_mem_mb == 0
        # assert s_filename == ""

    def test_validate_parameters_or_exit_m_malformed(self):

        a_s_parameters = ["cmemgzip", "-m"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            b_simulate, i_mem_mb, s_filename = o_cmemgzip.validate_parameters_or_exit()

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

        # It will not get to here, an exit will be triggered first
        # assert b_simulate is False
        # assert i_mem_mb == 0
        # assert s_filename == ""

    def test_get_filename_from_parameters_no_parameters(self):

        a_s_parameters = []

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        s_filename, a_files = o_cmemgzip.get_filename_from_parameters()

        assert s_filename == ""
        assert len(a_files) == 0

    def test_get_filename_from_parameters_one_parameter(self):

        # Parameter 0 is the name of the .py script invoked
        a_s_parameters = ["cmemgzip", "myfile.txt"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        s_filename, a_files = o_cmemgzip.get_filename_from_parameters()

        assert s_filename == "myfile.txt"
        assert len(a_files) == 1

    def test_get_filename_from_parameters_one_parameter_with_dash_no_filename(self):

        # Parameter 0 is the name of the .py script invoked
        a_s_parameters = ["cmemgzip", "-m"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        s_filename, a_files = o_cmemgzip.get_filename_from_parameters()

        assert s_filename == ""
        assert len(a_files) == 0

    def test_get_parameter_m_mb_ok(self):
        # Parameter 0 is the name of the .py script invoked
        a_s_parameters = ["cmemgzip", "-m=500M"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        b_m_parameter_present, b_success, i_mem_mb = o_cmemgzip.get_parameter_m()

        assert b_m_parameter_present is True
        assert b_success is True
        assert i_mem_mb == 500

    def test_get_parameter_m_gb_ok(self):
        # Parameter 0 is the name of the .py script invoked
        a_s_parameters = ["cmemgzip", "-m=2G"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        b_m_parameter_present, b_success, i_mem_mb = o_cmemgzip.get_parameter_m()

        assert b_m_parameter_present is True
        assert b_success is True
        assert i_mem_mb == 2*1024

    def test_get_parameter_m_wrong_value(self):
        # Parameter 0 is the name of the .py script invoked
        a_s_parameters = ["cmemgzip", "-m=500"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        b_m_parameter_present, b_success, i_mem_mb = o_cmemgzip.get_parameter_m()

        assert b_m_parameter_present is True
        assert b_success is False
        assert i_mem_mb == 0

    def test_get_parameter_m_wrong_unit_value(self):
        # Parameter 0 is the name of the .py script invoked
        a_s_parameters = ["cmemgzip", "-m=500xM"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        b_m_parameter_present, b_success, i_mem_mb = o_cmemgzip.get_parameter_m()

        assert b_m_parameter_present is True
        assert b_success is False
        assert i_mem_mb == 0

    def test_get_parameter_m_not_added(self):
        # Parameter 0 is the name of the .py script invoked
        a_s_parameters = ["cmemgzip"]

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        b_m_parameter_present, b_success, i_mem_mb = o_cmemgzip.get_parameter_m()

        assert b_m_parameter_present is False
        assert b_success is False
        assert i_mem_mb == 0

    def test_compress_loading_file_completely_in_memory_ok(self, monkeypatch):

        a_s_parameters = ["cmemgzip"]
        s_file = "spaces.txt"

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        def mock_fileutils_read_binary(o_fileutils, s_file):
            # We return the bytes of the imaginary file
            b_result = True
            s_file_contents = " " * 1000
            by_file_contents = bytes(s_file_contents, encoding="ascii")

            return b_result, by_file_contents

        monkeypatch.setattr(FileUtils, 'read_binary', mock_fileutils_read_binary)


        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        by_compressed, i_bytes_read = o_cmemgzip.compress_loading_file_completely_in_memory(i_filesize=1000,
                                                                                            s_file=s_file,
                                                                                            s_filesize_in_bytes="1,000")

        # Compress using gzip
        by_compressed_gzip = gzip.compress(bytes(" " * 1000, encoding="ascii"), compresslevel=9)

        assert i_bytes_read == 1000
        assert by_compressed == by_compressed_gzip

    def test_compress_loading_file_completely_in_memory_file_not_found(self):
        pass

    def test_compress_loading_file_completely_in_memory_sizes_dont_match(self):
        pass

    def test_compress_loading_file_completely_in_memory_exception_compressing(self):
        pass

    def test_verify_access_and_get_filesize_or_exit_exit(self, monkeypatch):

        s_file = "spaces.txt"
        a_s_parameters = ["cmemgzip", "spaces.txt"]
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        def mock_fileutils_get_file_size_in_bytes(o_fileutils, s_file):
            # We return the bytes of the imaginary file
            b_result = False
            i_filesize = 0

            return b_result, i_filesize

        monkeypatch.setattr(FileUtils, 'get_file_size_in_bytes', mock_fileutils_get_file_size_in_bytes)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            i_filesize = o_cmemgzip.verify_access_and_get_filesize_or_exit(s_file)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

    def test_verify_access_and_get_filesize_or_exit_ok(self, monkeypatch):

        s_file = "spaces.txt"
        a_s_parameters = ["cmemgzip", "spaces.txt"]
        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        def mock_fileutils_get_file_size_in_bytes(o_fileutils, s_file):
            # We return the bytes of the imaginary file
            b_result = True
            i_filesize = 1000

            return b_result, i_filesize

        monkeypatch.setattr(FileUtils, 'get_file_size_in_bytes', mock_fileutils_get_file_size_in_bytes)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)
        i_filesize = o_cmemgzip.verify_access_and_get_filesize_or_exit(s_file)

        assert i_filesize == 1000

    def test_compress_simulate_all_in_memory(self, monkeypatch):

        # No write to drive, simulate, dry-run

        a_s_parameters = ["cmemgzip", "spaces.txt"]
        s_file = "spaces.txt"

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)
        o_pythonutils = PythonUtils()

        def mock_fileutils_read_binary(o_fileutils, s_file):
            # We return the bytes of the imaginary file
            b_result = True
            s_file_contents = " " * 1000
            by_file_contents = bytes(s_file_contents, encoding="ascii")

            return b_result, by_file_contents

        monkeypatch.setattr(FileUtils, 'read_binary', mock_fileutils_read_binary)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils, o_pythonutils=o_pythonutils)

        def mock_fileutils_verify_access_and_get_filesize_or_exit(o_cmemgzip, s_file):

            i_file_size = 1000

            return i_file_size

        monkeypatch.setattr(Cmemgzip, 'verify_access_and_get_filesize_or_exit', mock_fileutils_verify_access_and_get_filesize_or_exit)

        # with pytest.raises(SystemExit) as pytest_wrapped_e:
        #     o_cmemgzip.compress(s_file, b_simulate=True, i_mem_mb=0)

        # assert pytest_wrapped_e.type == SystemExit
        # assert pytest_wrapped_e.value.code == 0
        i_return = o_cmemgzip.compress(s_file, b_simulate=True, i_mem_mb=0)

        assert i_return == 0

    def test_compress_simulate_blocks_ioerror(self, monkeypatch):

        # No write to drive, simulate, dry-run

        a_s_parameters = ["cmemgzip", "m=1M", "spaces.txt"]
        s_file = "spaces.txt"

        o_fileutils = FileUtils()
        o_stringutils = StringUtils()
        o_datetime = DateTimeUtils()
        o_argutils = ArgsUtils(a_s_args=a_s_parameters)
        o_screenutils = ScreenUtils(b_support_colors=True)

        def mock_fileutils_read_binary(o_fileutils, s_file):
            # We return the bytes of the imaginary file
            b_result = True
            s_file_contents = " " * 1000
            by_file_contents = bytes(s_file_contents, encoding="ascii")

            return b_result, by_file_contents

        monkeypatch.setattr(FileUtils, 'read_binary', mock_fileutils_read_binary)

        o_cmemgzip = Cmemgzip(o_fileutils=o_fileutils, o_stringutils=o_stringutils, o_datetime=o_datetime, o_argutils=o_argutils, o_screenutils=o_screenutils)

        def mock_fileutils_verify_access_and_get_filesize_or_exit(o_cmemgzip, s_file):

            i_file_size = 1000

            return i_file_size

        monkeypatch.setattr(Cmemgzip, 'verify_access_and_get_filesize_or_exit', mock_fileutils_verify_access_and_get_filesize_or_exit)

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            o_cmemgzip.compress(s_file, b_simulate=True, i_mem_mb=1)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1
