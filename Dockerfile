FROM ubuntu:20.04

MAINTAINER Carles

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y python3 python3-pip vim mc && apt-get clean

ENV CMEMGZIP /var/cmemgzip

RUN mkdir -p $CMEMGZIP

COPY cmemgzip.py $CMEMGZIP

RUN ln -s /var/cmemgzip/cmemgzip.py /bin/cmemgzip
